﻿using UnityEngine;
using System.Collections;

public class SuicideEnemy : Enemy 
{

	// Use this for initialization
	void Start () 
	{
		health = 5  + ((Global.level - 1) * 3);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void FixedUpdate()
	{
		rotateToPlayer();
		transform.Translate(Vector2.up * 4 * Time.deltaTime);
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			collision.gameObject.SendMessage("drainPower");
			collision.gameObject.SendMessage("takeDamage", 10);
			destoryThis();
		}
	}
}
