﻿using UnityEngine;
using System.Collections;

public class UI : MonoBehaviour 
{
	private Player player;
	private Manager manager;
	public GUIText healthText;
	public GUIText weaponText;
	public GUIText moneyText;

	public Texture2D bar;
	public Texture2D emptyBar;
	public Texture2D powerBar;

	public Texture2D laserBtn;
	public Texture2D damageBtn;
	public Texture2D powerBtn;
	public Texture2D rechargeBtn;


	public GUIStyle style;
	


	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		healthText.text = "Health: " + player.getHealth();

		if(player.getCurrentWeapon() == 0)
		{
			weaponText.text = "Weapon: Pistol";
		}
		else
		{
			weaponText.text = "Weapon: Laser";
		}

		moneyText.text = "Money: " + Global.money;
	}

	void OnGUI()
	{
		GUI.color = new Color(1,1,1,0.5f);
		GUI.BeginGroup (new Rect (Screen.width / 2 - 300, 20, 600, 50));
			GUI.DrawTexture(new Rect (0,0, 600, 50), emptyBar);
		GUI.DrawTexture(new Rect (0,0, manager.getCurrentLevelTime() * (600 / manager.getLevelTime()), 50), bar);
		GUI.EndGroup();

		GUI.BeginGroup (new Rect (Screen.width / 2 - 300, Screen.height - 70, 600, 50));
			GUI.DrawTexture(new Rect (0, 0, 600, 50), emptyBar);
			GUI.DrawTexture(new Rect (0,0, player.getPower() * (600 / player.getMaxPower()), 50), powerBar);
		GUI.color = Color.black;
		GUI.Label(new Rect(0, 0, 600, 50), "Power: " + player.getPower(), style);
		GUI.EndGroup();

		if(Global.shop)
		{
			GUI.color = new Color(1,1,1,1f);
			GUI.BeginGroup (new Rect (Screen.width / 2 - 300, Screen.height / 2 - 250, 600, 500));
			DrawQuad(new Rect (0, 0, 600, 500), Color.white);
			GUI.Label(new Rect(0, 10, 600, 20), "Shop", style);

			//Laser btn
			if(Global.hasLaser)
			{
				GUI.enabled = false;
			}
			if(GUI.Button(new Rect(225, 50, 150,150), laserBtn))
			{
				if(Global.money >= 300)
				{
					Global.money -= 300;
					Global.hasLaser = true;
				}

			}
			if(!Global.hasLaser)
			{
				GUI.Label(new Rect(0, 210, 600, 20), "Cost 300", style);
			}
			else
			{
				GUI.Label(new Rect(0, 210, 600, 20), "Owned", style);
			}
			GUI.enabled = true;


			//Damage btn
			if(Global.damageLvl == 4)
			{
				GUI.enabled = false;
			}
			if(GUI.Button(new Rect(50, 250, 150,150), damageBtn))
			{
				if(Global.money >= Global.damageLvl * 100)
				{
					Global.money -= Global.damageLvl * 100;
					Global.damageLvl++;
				}
			}
			GUI.Label(new Rect(-175, 410, 600, 20), "Lvl " + Global.damageLvl + ": Cost " + Global.damageLvl * 100, style);
			GUI.enabled = true;


			//Pwer btn
			if(Global.powerLvl == 4)
			{
				GUI.enabled = false;
			}
			if(GUI.Button(new Rect(225, 250, 150,150), powerBtn))
			{
				if(Global.money >= Global.powerLvl * 100)
				{
					Global.money -= Global.powerLvl * 100;
					Global.powerLvl++;
				}
			}
			GUI.Label(new Rect(0, 410, 600, 20), "Lvl " + Global.powerLvl + ": Cost " + Global.powerLvl * 100, style);
			GUI.enabled = true;
			//Recharge btn
			if(Global.powerChargeLvl == 4)
			{
				GUI.enabled = false;
			}
			if(GUI.Button(new Rect(400, 250, 150,150), rechargeBtn))
			{
				if(Global.money >= Global.powerChargeLvl * 100)
				{
					Global.money -= Global.powerChargeLvl * 100;
					Global.powerChargeLvl++;
				}
			}
			GUI.Label(new Rect(175, 410, 600, 20), "Lvl " + Global.powerChargeLvl + ": Cost " + Global.powerChargeLvl * 100, style);
			GUI.enabled = true;

			if(GUI.Button(new Rect(250, 450, 100, 30), "OK"))
			{
				manager.pause();
				Global.shop = false;
			}
			GUI.EndGroup();
		}
	}
	void DrawQuad(Rect position, Color color) 
	{
		Texture2D texture = new Texture2D(1, 1);
		texture.SetPixel(0,0,color);
		texture.Apply();
		GUI.skin.box.normal.background = texture;
		GUI.Box(position, GUIContent.none);
	}
}
