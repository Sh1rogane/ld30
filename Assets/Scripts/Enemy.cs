﻿using UnityEngine;
using System.Collections;

public class Enemy : BasicObject
{
	protected int health = 10;
	protected int worth = 10;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	public void rotateToPlayer()
	{
		GameObject player = GameObject.Find("Player");
		
		Vector3 playerPosition = player.transform.position;
		playerPosition.z = -10;
		Quaternion rot = Quaternion.LookRotation(transform.position - playerPosition, Vector3.forward);
		
		transform.rotation = rot;
		transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
		rigidbody2D.angularVelocity = 0;
	}
	public void destoryThis()
	{
		ParticleHelper.instance.createExplosion(transform.position);
		Destroy(this.gameObject);
	}
	public void takeDamage(int amount)
	{
		health -= amount;
		audio.Play();
		if(health <= 0)
		{
			Global.money += worth + ((Global.level - 1) * 5);
			destoryThis();
		}

	}
	public void takeLaserDamage(int amount)
	{
		health -= amount;
		if(health <= 0)
		{
			Global.money += worth + ((Global.level - 1) * 5);
			destoryThis();
		}
	}
}
