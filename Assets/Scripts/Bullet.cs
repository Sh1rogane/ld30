﻿using UnityEngine;
using System.Collections;

public class Bullet : BasicObject 
{
	public int damage = 10;
	public float bulletSpeed = 5;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Translate(Vector2.up * bulletSpeed * Time.deltaTime);

		if(this.transform.position.x > 10 || this.transform.position.x < -10)
		{
			Destroy(this.gameObject);
		}
		if(this.transform.position.y > 10 || this.transform.position.y < -10)
		{
			Destroy(this.gameObject);
		}
	}
	void OnCollisionEnter2D(Collision2D collision)
	{
		Destroy(this.gameObject);
		if(collision.gameObject.tag == "Enemy")
		{
			collision.gameObject.SendMessage("takeDamage", 10 + ((Global.damageLvl - 1) * 5));
		}

	}

}
