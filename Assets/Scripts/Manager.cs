﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour 
{
	public static bool paused = false;

	private int level = 1;
	private Player player;

	private float spawnTimer = 0f;

	public GameObject enemy;
	public GameObject stealthyEnemy;
	public GameObject dogeEnemy;
	public GameObject invertedEnemy;
	public GameObject suicideEnemy;

	private float levelTime = 60f;
	private float currentLevelTime = 60f;

	private bool level3FirstTime = true;
	private bool level6FirstTime = true;
	private bool level10FirstTime = true;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(Global.level == 1)
		{
			level1();
		}
		else if(Global.level == 2)
		{
			level2();
		}
		else if(Global.level == 3)
		{
			level3();
		}
		else if(Global.level == 4)
		{
			level4();
		}
		else if(Global.level == 5)
		{
			level5();
		}
		else if(Global.level == 6)
		{
			level6();
		}
		else if(Global.level == 7)
		{
			level7();
		}
		else if(Global.level == 8)
		{
			level8();
		}
		else if(Global.level == 9)
		{
			level9();
		}
		else if(Global.level == 10)
		{
			level10();
		}
		if(currentLevelTime <= 0 && !paused)
		{
			pause();
		}
	}
	private void level1()
	{
		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 5.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(enemy, getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}
	private void level2()
	{
		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 5.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(randomEnemy(2), getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}
	private void level3()
	{
		if(level3FirstTime)
		{
			level3FirstTime = false;
			for(int i = 0; i < 4; i++)
			{
				GameObject temp = Instantiate(suicideEnemy, getRandomPos(), new Quaternion()) as GameObject;
				temp.GetComponent<BasicObject>().setDimension(player.getDimension());
			}
		}

		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 5.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(randomEnemy(3), getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}
	private void level4()
	{
		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 3.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(randomEnemy(3), getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}
	private void level5()
	{
		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 5.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(randomEnemy(4), getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}private void level6()
	{
		if(level6FirstTime)
		{
			level6FirstTime = false;
			for(int i = 0; i < 3; i++)
			{
				GameObject temp = Instantiate(invertedEnemy, getRandomPos(), new Quaternion()) as GameObject;
				temp.GetComponent<BasicObject>().setDimension(player.getDimension());
			}
		}
		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 5.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(randomEnemy(5), getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}
	private void level7()
	{
		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 5.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(randomEnemy(5), getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}
	private void level8()
	{
		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 3.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(randomEnemy(5), getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}
	private void level9()
	{
		spawnTimer += Time.deltaTime;
		currentLevelTime -= Time.deltaTime;
		if(spawnTimer >= 3.0f)
		{
			spawnTimer = 0;
			GameObject temp = Instantiate(randomEnemy(5), getRandomPos(), new Quaternion()) as GameObject;
			temp.GetComponent<BasicObject>().setDimension(randomBool());
		}
	}
	private void level10()
	{
		if(level10FirstTime)
		{
			level10FirstTime = false;
		}
	}

	public void pause()
	{
		paused = !paused;
		if(paused)
		{
			Global.shop = true;
			Time.timeScale = 0;
		}
		else
		{
			Global.shop = false;
			Time.timeScale = 1;
			currentLevelTime = 60;
			Global.level++;
		}
	}
	public float getLevelTime()
	{
		return levelTime;
	}
	public float getCurrentLevelTime()
	{
		return currentLevelTime;
	}
	private GameObject randomEnemy(int numEnemies)
	{
		int r = Random.Range(1, numEnemies + 1);
		if(r == 1)
		{
			return enemy;
		}
		else if(r == 2)
		{
			return stealthyEnemy;
		}
		else if(r == 3)
		{
			return suicideEnemy;
		}
		else if(r == 4)
		{
			return dogeEnemy;
		}
		else if(r == 5)
		{
			return invertedEnemy;
		}
		return enemy;
	}
	private bool randomBool()
	{
		return Random.value > 0.5f;
	}
	private Vector2 getRandomPos()
	{
		int dir = Random.Range(1,4);
		Vector2 vector = new Vector2();

		//top
		if(dir == 1)
		{
			vector.y = 6;
			vector.x = Random.Range(-5, 6);
		}
		//bottom
		else if(dir == 2)
		{
			vector.y = -6;
			vector.x = Random.Range(-5, 6);
		}
		//right
		else if(dir == 3)
		{
			vector.y = Random.Range(-5, 5);
			vector.x = 8;
		}
		//left
		else if(dir == 4)
		{
			vector.y = Random.Range(-5, 5);
			vector.x = -8;
		}
		return vector;
	}
	void FixedUpdate()
	{
		Object[] go = GameObject.FindObjectsOfType(typeof(BasicObject));
		foreach(Object o in go)
		{
			BasicObject b = (BasicObject) o;
			if(b != player)
			{
				if(player.getDimension())
				{
					if(b.getDimension())
					{
						b.setTransparent(1f);
					}
					else
					{
						b.setTransparent(0.5f);
					}
				}
				else
				{
					if(b.getDimension())
					{
						b.setTransparent(0.5f);
					}
					else
					{
						b.setTransparent(1f);
					}
				}
			}

		}
	}

	public void generateLevel()
	{
		if(level == 1)
		{

		}
		else
		{

		}
	}
}
