﻿using UnityEngine;
using System.Collections;

public class FollowEnemy : Enemy 
{

	// Use this for initialization
	void Start () 
	{
		health = 50  + ((Global.level - 1) * 5);
	}
	// Update is called once per frame
	void Update()
	{

	}

	void FixedUpdate () 
	{
		rotateToPlayer();
		transform.Translate(Vector2.up * 3 * Time.deltaTime);
	}
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			collision.gameObject.SendMessage("takeDamage", 10);
			destoryThis();
		}
	}
}
