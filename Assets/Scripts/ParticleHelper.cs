﻿using UnityEngine;
using System.Collections;

public class ParticleHelper : MonoBehaviour 
{
	public static ParticleHelper instance;

	public ParticleSystem ps;
	public ParticleSystem exp;
	public ParticleSystem laserHit;
	public ParticleSystem laserLoad;
	// Use this for initialization
	void Start () 
	{
		instance = this;
	}

	public void createParticle(Vector3 position)
	{
		ParticleSystem p = Instantiate(ps, position, Quaternion.identity) as ParticleSystem;
		Destroy(p.gameObject, p.startLifetime);
	}

	public void createExplosion(Vector3 position)
	{
		ParticleSystem p = Instantiate(exp, position, Quaternion.identity) as ParticleSystem;
		Destroy(p.gameObject, p.startLifetime);
	}

	public void createLaserHit(Vector3 position)
	{
		ParticleSystem p = Instantiate(laserHit, position, Quaternion.identity) as ParticleSystem;
		Destroy(p.gameObject, p.startLifetime);
	}
	public void createLaserLoad(Vector3 position)
	{
		ParticleSystem p = Instantiate(laserLoad, position, Quaternion.identity) as ParticleSystem;
		Destroy(p.gameObject, p.startLifetime);
	}
}
