﻿using UnityEngine;
using System.Collections;

public class Global
{
	public static bool hasLaser = false;
	public static int damageLvl = 1;
	public static int powerLvl = 1;
	public static int powerChargeLvl = 1;
	public static int powerCooldownLvl = 1;

	public static int money = 0;

	public static bool shop = false;

	public static int level = 1;
}
