﻿using UnityEngine;
using System.Collections;

public class Player : BasicObject 
{
	private Vector3 mousePosition;

	public GameObject bullet;
	public float speed = 3;
	public AudioClip changeSound;

	private int maxPower = 100;
	private int power = 100;
	private bool powerUsed = false;
	private float powerLoadTimer = 0f;

	private int health = 100;

	private enum Weapons {Bullet, Laser, Slow, Explosion};
	private int currentWeapon = 0;

	private float bulletFireRate = 0.25f;
	private float fireTime = 0f;

	private bool usePower;
	private bool laserFire = false;
	private bool laserLoad = true;
	private float laserLoadTime = 1f;
	private float laserLoadTimer = 0f;

	private LineRenderer line;

	// Use this for initialization
	void Start () 
	{
		line = GetComponent<LineRenderer>();
		line.SetVertexCount(2);
	}
	
	// Update is called once per frame
	void Update () 
	{
		laserFire = false;
		maxPower = 100 + ((Global.powerLvl - 1) * 50);

		fireTime += Time.deltaTime;
		if(powerUsed)
		{
			powerLoadTimer += Time.deltaTime;
			if(powerLoadTimer >= 1.0f)
			{
				powerLoadTimer = 0;
				powerUsed = false;
			}
		}
		if(!Manager.paused)
		{
			if(Input.GetKeyDown(KeyCode.Alpha1))
			{
				currentWeapon = (int)Weapons.Bullet;
			}
			else if(Input.GetKeyDown(KeyCode.Alpha2))
			{
				currentWeapon = (int)Weapons.Laser;
			}
			if(Input.GetKeyDown(KeyCode.Space) && power >= 30)
			{
				AudioSource.PlayClipAtPoint(changeSound, this.transform.position);
				ParticleHelper.instance.createParticle(this.transform.position);
				lowerPower(30);
				if(getDimension())
				{
					setDimension(false);
				}
				else
				{
					setDimension(true);
				}
				
			}
			if(Input.GetButton("Fire1"))
			{
				if(currentWeapon == (int)Weapons.Bullet)
				{
					if(fireTime >= bulletFireRate  && power >= 5)
					{
						fireTime = 0;
						fire();
						lowerPower(5);
					}

				}
				else if(currentWeapon == (int)Weapons.Laser && power > 0)
				{
					laserFire = true;
				}
			}
			if(Input.GetButtonUp("Fire1"))
			{
				laserLoad = true;
				laserLoadTimer = 0;
			}
		}


	}
	private LayerMask getLaserLayer()
	{
		if(getDimension())
		{
			return LayerMask.NameToLayer("Dimension1");
		}
		else
		{
			return LayerMask.NameToLayer("Dimension2");
		}
	}
	void FixedUpdate()
	{
		line.enabled = false;
		if(laserFire)
		{
			laser ();
		}
		if(power < maxPower && !powerUsed)
		{
			power += Global.powerChargeLvl;
		}
		if(power > maxPower)
		{
			power = maxPower;
		}
		var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		Quaternion rot = Quaternion.LookRotation(transform.position - mousePosition, Vector3.forward);

		transform.rotation = rot;
		transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
		rigidbody2D.angularVelocity = 0;
		this.rigidbody2D.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, Input.GetAxis("Vertical") * speed);
	}
	public void lowerPower(int amount)
	{
		powerLoadTimer = 0;
		powerUsed = true;
		power -= amount;

		if(power < 0)
		{
			power = 0;
		}
	}
	public int getCurrentWeapon()
	{
		return currentWeapon;
	}
	public int getMaxPower()
	{
		return maxPower;
	}
	public int getPower()
	{
		return power;
	}
	private void laser()
	{
		if(laserLoad)
		{
			ParticleHelper.instance.createLaserLoad(transform.position + transform.up / 3);
			laserLoadTimer += Time.deltaTime;
			if(laserLoadTimer >= laserLoadTime)
			{
				laserLoad = false;
			}
		}
		else
		{
			usePower = !usePower;
			if(usePower)
			{
				ParticleHelper.instance.createLaserLoad(transform.position + transform.up / 3);
				lowerPower(1);
			}

			line.enabled = true;
			RaycastHit2D hit = Physics2D.Raycast(this.transform.position, transform.up, Mathf.Infinity, 1<<getLaserLayer().value);
			if(hit.collider != null)
			{
				line.SetPosition(0, transform.position + transform.up / 3);
				line.SetPosition(1, hit.point);
				hit.collider.gameObject.SendMessage("takeLaserDamage", Global.damageLvl);
				ParticleHelper.instance.createLaserHit(hit.point);
			}
			else
			{
				line.SetPosition(0, transform.position + transform.up / 3);
				line.SetPosition(1, transform.position + transform.up * 10);
			}
		}
	}
	private void fire()
	{
		var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector3 dir = mousePosition - this.transform.position;
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

		GameObject tempBullet = (Instantiate(bullet, transform.position, Quaternion.AngleAxis(angle - 90, Vector3.forward))) as GameObject;
		tempBullet.transform.Translate(Vector2.up * 0.6f);
		Bullet b = tempBullet.GetComponent<Bullet>();
		b.setDimension(this.getDimension());

		this.audio.Play();
	}
	public int getHealth()
	{
		return health;
	}
	public void drainPower()
	{
		lowerPower(100);
	}
	void OnCollisionStay2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Enemy")
		{
			this.health--;
		}
	}

	new public void setDimension(bool d)
	{
		if(d == true)
		{
			if(this.gameObject.layer != 10)
			{
				this.gameObject.layer = 10;
			}
			
		}
		else
		{
			if(this.gameObject.layer != 11)
			{
				this.gameObject.layer = 11;
			}
		}
	}
	new public bool getDimension()
	{
		return this.gameObject.layer == 10 ? true : false;
	}
	public void takeDamage(int amount)
	{
		this.health -= amount;
	}
}























