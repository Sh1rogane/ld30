﻿using UnityEngine;
using System.Collections;

public class BasicObject : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void setDimension(bool d)
	{
		if(d == true)
		{
			if(this.gameObject.layer != 8)
			{
				this.gameObject.layer = 8;
			}

		}
		else
		{
			if(this.gameObject.layer != 9)
			{
				this.gameObject.layer = 9;
			}
		}
	}
	public bool getDimension()
	{
		return this.gameObject.layer == 8 ? true : false;
	}
	public void setTransparent(float value)
	{
		this.renderer.material.color = new Color(this.renderer.material.color.r, this.renderer.material.color.g, this.renderer.material.color.b, value);
	}
}
