﻿using UnityEngine;
using System.Collections;

public class InvertedEnemy : Enemy 
{

	// Use this for initialization
	void Start () 
	{
		health = 10  + ((Global.level - 1) * 3);
		worth = 30;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void FixedUpdate()
	{
		rotateToPlayer();
		transform.Translate(Vector2.up * 1 * Time.deltaTime);

		GameObject player = GameObject.Find("Player");
		Player p = player.GetComponent<Player>();

		if(getDimension() == p.getDimension())
		{
			ParticleHelper.instance.createParticle(this.transform.position);
			setDimension(!p.getDimension());
		}
	}
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			collision.gameObject.SendMessage("takeDamage", 10);
			destoryThis();
		}
	}
}
