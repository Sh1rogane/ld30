﻿using UnityEngine;
using System.Collections;

public class StealthyEnemy : Enemy 
{
	private float stealthTimer = 0;
	private bool stealth = false;
	// Use this for initialization
	void Start () 
	{
		health = 30 + ((Global.level - 1) * 5);
		worth = 20;
	}
	
	// Update is called once per frame
	void Update () 
	{
		rotateToPlayer();
		GameObject player = GameObject.Find("Player");
		Player p = player.GetComponent<Player>();

		float dist = Vector2.Distance(this.transform.position, player.transform.position);
		if(dist <= 3f)
		{
			stealth = false;
			transform.Translate(Vector2.up * 3 * Time.deltaTime);
			if(getDimension() != p.getDimension())
			{
				ParticleHelper.instance.createParticle(this.transform.position);
				setDimension(p.getDimension());
			}
		}
		else
		{
			transform.Translate(Vector2.up * 2 * Time.deltaTime);
			if(getDimension() == p.getDimension())
			{
				stealth = false;
			}
			if(!stealth)
			{
				stealthTimer += Time.deltaTime;
			}
			if(stealthTimer >= 0.5f)
			{
				stealth = true;
				stealthTimer = 0f;
				ParticleHelper.instance.createParticle(this.transform.position);
				setDimension(!p.getDimension());
			}

		}

	}
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			destoryThis();
			collision.gameObject.SendMessage("takeDamage", 10);
		}
	}
}
