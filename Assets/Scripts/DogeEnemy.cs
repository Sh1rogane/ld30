﻿using UnityEngine;
using System.Collections;

public class DogeEnemy : Enemy 
{
	private bool doged;
	// Use this for initialization
	void Start () 
	{
		health = 20 + ((Global.level - 1) * 5);
		worth = 30;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void FixedUpdate()
	{
		rotateToPlayer();
		transform.Translate(Vector2.up * 2 * Time.deltaTime);
		if(!doged)
		{
			foreach(GameObject b in GameObject.FindGameObjectsWithTag("Bullet"))
			{
				BasicObject bb = b.GetComponent<BasicObject>();
				if(bb.getDimension() == getDimension())
				{
					float dist = Vector2.Distance(this.transform.position, b.transform.position);
					if(dist < 1f)
					{
						doged = true;
						ParticleHelper.instance.createParticle(this.transform.position);
						setDimension(!getDimension());
						break;
					}
				}
				
			}
		}


	}
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			collision.gameObject.SendMessage("takeDamage", 10);
			destoryThis();
		}
	}
}
